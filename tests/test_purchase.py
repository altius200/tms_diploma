import time

from selenium.common import StaleElementReferenceException

from src.base.base_test import BaseTest
import time


class TestPurchase(BaseTest):

    def test_purchase_with_empty_card(self):
        self.main_page.open(self.main_page.url)
        self.main_page.click(self.purchase_page.cart_button)
        time.sleep(0.5)
        assert self.purchase_page.get_text(
            self.purchase_page.empty_cart_message) == "You have no items in your shopping cart.", \
            "Empty cart message is not displayed"

    def test_add_item_from_main_page(self):
        self.main_page.open(self.main_page.url)
        self.purchase_page.add_item_from_main_page()
        assert '1' in self.purchase_page.check_added_element_counter(), "Item is not added to card"

    def test_add_item_from_detailed_page(self):
        self.purchase_page.add_2_items_from_detailed_page()
        assert '2' in self.purchase_page.check_added_element_counter(), "Item is not added to card"

    def test_delete_item_from_card(self):
        self.main_page.open(self.main_page.url)
        self.purchase_page.add_item_from_main_page()
        self.purchase_page.delete_item_from_card()
        assert self.purchase_page.emtpy_cart_message() == "You have no items in your shopping cart.", \
            "Empty cart message is not displayed"

    def test_validation_message_without_size_and_color(self):
        self.purchase_page.open(self.purchase_page.item_detailed_page_url)
        self.purchase_page.click(self.purchase_page.detailed_page_add_to_cart_button)
        assert self.purchase_page.get_text(self.purchase_page.validation_message_about_size) and \
               self.purchase_page.get_text(self.purchase_page.validation_message_about_color) == \
               "This is a required field.", "Validation message is not displayed"

