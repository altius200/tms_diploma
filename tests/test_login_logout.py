import time

from src.base.base_test import BaseTest


class TestLogIn(BaseTest):
    def test_login(self):
        self.login_page.open_login_form()
        assert self.login_page.get_text(self.login_page.title) == 'Customer Login', \
            f"Login failed, got {self.login_page.get_text(self.login_page.title)}"
        self.login_page.log_in()
        time.sleep(0.3)
        assert self.login_page.get_text(self.login_page.logged_in) == 'Welcome, Veronica Costello!', f"Login failed"

    def test_logout(self):
        self.login_page.open_login_form()
        self.login_page.log_in()
        self.login_page.log_out()
        assert self.login_page.get_text(self.login_page.log_out_message) == 'You are signed out', f"Logout failed"
        assert self.login_page.log_out_url == self.login_page.get_current_url(), f"Logout failed"
        self.login_page.element_is_not_present(self.login_page.log_out_message)
        assert self.main_page.url == self.main_page.get_current_url(), f"Logout failed"
