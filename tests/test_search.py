import time

from src.base.base_test import BaseTest


class TestSearch(BaseTest):

    def test_valid_search(self):
        self.main_page.open(self.main_page.url)
        self.main_page.input_search_field("Impulse Duffle")
        self.main_page.click_search_button()
        results = self.search_page.get_search_valid_result()
        assert len(results) == 3

    def test_invalid_search(self):
        self.main_page.open(self.main_page.url)
        self.main_page.input_search_field("invalid search")
        self.main_page.click_search_button()

        assert self.search_page.invalid_search() == self.search_page.INVALID_SEARCH_TEXT, \
            f'Expected text: {self.search_page.INVALID_SEARCH_TEXT}, but got {self.search_page.invalid_search()}'
