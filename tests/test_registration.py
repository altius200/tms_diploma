import time

import pytest

from src.base.base_test import BaseTest


class TestRegistration(BaseTest):

    def test_registration_page_title(self):
        self.main_page.open(self.main_page.URL)
        self.main_page.click(self.main_page.create_account_button)
        assert self.registration_page.get_registration_title() == 'Create New Customer Account', \
            f"Registration page title is not correct, got {self.registration_page.get_registration_title()}"

    def test_valid_user_registration(self, fake_user):
        self.registration_page.register_new_user(fake_user)
        assert self.registration_page.get_register_message() == f"Thank you for registering with Main Website Store.", \
            f"Registration failed for user {self.fake_user.first_name} {self.fake_user.last_name}"
        assert self.registration_page.my_account_url == self.registration_page.get_current_url(), f'User was not registered'


    @pytest.mark.skip(reason='there is a bug')
    def test_registration_form_validation(self):
        self.registration_page.open_registration_page()
        time.sleep(0.3)
        self.registration_page.click_register_button()
        assert self.registration_page.registration_form_validation() == True, 'Form validation failed'
