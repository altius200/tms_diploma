import time

from src.base.base_test import BaseTest


class TestCheckout(BaseTest):

    def test_checkout_form_validation(self):
        self.purchase_page.proceed_to_checkout_from_detail_page()
        self.checkout_page.click_continue_button()
        assert self.checkout_page.shipping_method_validation() == ("The shipping method is missing. Select the "
                                                                   "shipping method and try again."), \
            "Validation message is not correct"

    def test_validation_checkout_form(self):
        self.purchase_page.proceed_to_checkout_from_detail_page()
        self.checkout_page.click_continue_with_empty_form()


    def test_address_validation_for_logged_user(self):
        self.login_page.open_login_form()
        self.login_page.log_in()
        self.purchase_page.proceed_to_checkout_from_detail_page()
        self.checkout_page.click(self.checkout_page.shipping_method_5_dollars)
        self.checkout_page.click_continue_button()
        assert self.checkout_page.shipping_address_text_validation() == True, "Shipping address is not correct"

    def test_purchase_by_logged_user(self):
        self.login_page.open_login_form()
        self.login_page.log_in()
        self.purchase_page.proceed_to_checkout_from_detail_page()
        self.checkout_page.click(self.checkout_page.shipping_method_5_dollars)
        self.checkout_page.click_continue_button()
        time.sleep(0.3)
        self.checkout_page.click(self.checkout_page.place_order_button)
        assert self.checkout_page.secsess_message_text() == "Thank you for your purchase!", "Purchase is not successful"
