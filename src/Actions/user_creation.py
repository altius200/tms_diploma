from faker import Faker

class User:
    def __init__(self, first_name, last_name, email, password):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password

    @classmethod
    def create_fake_user(cls):
        fake = Faker()
        first_name = fake.user_name()
        last_name = fake.last_name()
        email = fake.email()
        password = fake.password(length=8, special_chars=True, upper_case=True)
        return cls(first_name, last_name, email, password)
