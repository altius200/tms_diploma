from selenium.webdriver.common.by import By
from ..Data.created_user import USER_INFO_FOR_PURCHASE_PAGE as user_info
from src.base.Base_page import BasePage


class CheckoutPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

        # web elements for checkout page
        self.shipping_method = (By.XPATH, "(//td[@class='col col-method'])[1]")
        self.continue_button = (By.XPATH, "//button[@class='button action continue primary']")
        self.place_order_button = (By.XPATH, "//button[@title='Place Order']")


        # web elements for shipping method
        self.shipping_method_5_dollars = (By.XPATH, "//input[@name='ko_unique_1']")
        self.shipping_method_10_dollars = (By.XPATH, "//input[@name='ko_unique_1']")

        # web elements for validation
        self.shipping_method_validation_text = (By.XPATH, "//span[@data-bind='text: errorValidationMessage()']")
        self.success_message = (By.XPATH, "//span[contains(.,'Thank you for your purchase!')]")


        # web elements for shipping address
        self.shipping_address = (By.XPATH, "//div[@class='shipping-address-item selected-item']")

    def click_continue_button(self):
        self.click(self.continue_button)

    def shipping_method_validation(self):
        return self.get_text(self.shipping_method_validation_text)

    def click_continue_with_empty_form(self):
        self.click(self.shipping_method_5_dollars)
        self.click(self.continue_button)

    def shipping_address_text_validation(self):
        for element in user_info:
            if element in self.get_text(self.shipping_address):
                return True
            else:
                return False

    def secsess_message_text(self):
        return self.get_text(self.success_message)
