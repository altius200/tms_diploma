from selenium.webdriver.common.by import By

from src.base.Base_page import BasePage


class MainPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.url = self.URL
        # web elements from header
        self.create_account_button = (By.XPATH, "//div[@class='panel header']//a[.='Create an Account']")
        self.login_button = (By.XPATH, "//div[@class='panel header']//a[contains(text(),'Sign In')]")
        self.men_category = (By.XPATH, "//*[ends-with(@href, '/men.html')]")
        # search web elements
        self.search_field = (By.XPATH, "//input[@id='search']")
        self.search_button = (By.XPATH, "//button[@title='Search']")

    def click_create_account_button(self):
        self.click(self.create_account_button)

    # for search
    def input_search_field(self, text):
        self.input(self.search_field, text)

    def click_search_button(self):
        self.click(self.search_button)


