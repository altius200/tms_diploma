from selenium.webdriver.common.by import By

from src.base.Base_page import BasePage


class SearchPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.url = self.URL

        # web elements from search page
        self.search_valid_result = (By.XPATH, "//li[@class='item product product-item']")
        self.search_invalid_result = (By.XPATH, "//div[contains(text(),'Your search returned no results.')]")
        # text for invalid search
        self.INVALID_SEARCH_TEXT = 'Your search returned no results.'

    def get_search_valid_result(self):
        return self.find_elements(self.search_valid_result)

    def invalid_search(self):
        return self.get_text(self.search_invalid_result)
