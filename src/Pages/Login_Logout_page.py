import time
from selenium.webdriver.common.by import By
from src.base.Base_page import BasePage


class LoginPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        # url
        self.url = self.URL + 'customer/account/login/referer/aHR0cHM6Ly9tYWdlbnRvMi1kZW1vLm1hZ2ViaXQuY29tLw%2C%2C/'
        self.log_out_url = self.URL + 'customer/account/logoutSuccess/'
        # web elements for log in
        self.email = (By.XPATH, "//input[@id='email']")
        self.password = (By.XPATH, "//input[@id='pass']")
        self.login_button = (By.XPATH, "//button[@class='action login primary']")
        self.title = (By.XPATH, "//span[@data-ui-id='page-title-wrapper']")
        self.logged_in = (By.XPATH, "//div[@class='panel header']//span[@class='logged-in']")
        # web elements for log out
        self.drop_down_actions_list = (By.XPATH, "//div[@class='panel header']//span[@role='button']")
        self.log_out_button = (By.XPATH, "(//a[contains(text(),'Sign Out')])[1]")
        # self.log_out_message = (By.XPATH, "(//span[@class ='base'])[1]")
        self.log_out_message = (By.XPATH, "//*[.='You are signed out']")

        # login data ( because it is a demo site, the data is static)
        self.email_data = "roni_cost@example.com"
        self.password_data = "roni_cost3@example.com"

    def open_login_form(self):
        self.open(self.url)
        self.click(self.login_button)

    def log_in(self):
        self.input(self.email, self.email_data)
        self.input(self.password, self.password_data)
        self.click(self.login_button)

    def log_out(self):
        time.sleep(0.3)
        self.click(self.drop_down_actions_list)
        self.click(self.log_out_button)

    def element_is_not_present(self, locator):
        self.wait_until_element_is_not_displayed(locator)
