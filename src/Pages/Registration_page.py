from selenium.webdriver.common.by import By

from src.Actions.user_creation import User
from src.base.Base_page import BasePage


class RegistrationPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

        # URLs
        self.url = f"{self.URL}customer/account/create/"
        self.my_account_url = f"{self.URL}customer/account/"
        # web elements
        self.first_name_input = (By.XPATH, "//input[@id='firstname']")
        self.last_name_input = (By.XPATH, "//input[@id='lastname']")
        self.sign_up_for_newsletter_checkbox = (By.XPATH, "//input[@id='is_subscribed']")
        self.email_input = (By.XPATH, "//input[@id='email_address']")
        self.password_input = (By.XPATH, "//input[@id='password']")
        self.password_confirm_input = (By.XPATH, "//input[@id='password-confirmation']")
        self.register_button = (
            By.XPATH, "//button[@title='Create an Account']//span[contains(text(),'Create an Account')]")
        self.register_message = (By.XPATH, "//div[@class='message-success success message']")
        self.registration_page_title = (By.XPATH, "//h1[@class='page-title']")
        # text errors
        self.first_name_error = (By.XPATH, "//div[@id='firstname-error']")
        self.last_name_error = (By.XPATH, "//div[@id='lastname-error']")
        self.email_error = (By.XPATH, "//div[@id='email_address-error']")
        self.password_error = (By.XPATH, "//div[@id='password-error']")
        self.password_confirm_error = (By.XPATH, "//div[@id='password-confirmation-error']")
        # validation text
        self.validation_text = "This is a required field."

    def open_registration_page(self):
        self.open(self.url)

    def input_first_name(self, first_name):
        self.input(self.first_name_input, first_name)

    def input_last_name(self, last_name):
        self.input(self.last_name_input, last_name)

    def input_email(self, email):
        self.input(self.email_input, email)

    def input_password(self, password):
        self.input(self.password_input, password)

    def input_password_confirm(self, password):
        self.input(self.password_confirm_input, password)

    def click_register_button(self):
        self.click(self.register_button)

    def get_registration_title(self):
        return self.get_text(self.registration_page_title)

    def get_register_message(self):
        return self.get_text(self.register_message)

    def scroll_to_registration_button(self):
        self.scroll(self.register_button)

    def register_new_user(self, fake_user, ):
        self.open(self.url)
        self.input_first_name(fake_user.first_name)
        self.input_last_name(fake_user.last_name)
        self.input_email(fake_user.email)
        self.input_password(fake_user.password)
        self.input_password_confirm(fake_user.password)
        self.click_register_button()

    def registration_form_validation(self):
        error_elements = [self.first_name_error,
                          self.last_name_error,
                          self.email_error,
                          self.password_error,
                          self.password_confirm_error]
        for element in error_elements:
            if self.get_text(element) == self.validation_text:
                return True
            else:
                return False
