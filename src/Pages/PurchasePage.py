import time

from selenium.webdriver.common.by import By

from src.base.Base_page import BasePage


class PurchasePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

        # web elements for adding item to card on the main page
        self.element_to_buy = (By.XPATH, "//a[@href='https://magento2-demo.magebit.com/radiant-tee.html']/parent::div")
        self.size_xs = (By.XPATH, "(//div[contains(text(), 'XS')])[1]")
        self.color = (By.XPATH, "//div[@data-option-label='Blue']")
        self.add_to_cart_button = (By.XPATH, "(//button[@title='Add to Cart'])[1]")
        self.cart_button = (By.XPATH, "//div[@class='minicart-wrapper']")
        self.indication_of_added_item = (By.XPATH, "//a[@class='action showcart']//span[@class='counter qty']")
        self.delete_button = (By.XPATH, "//a[@title='Remove item']")
        self.delete_ok_button = (By.XPATH, "(//button[@class='action-primary action-accept'])[1]")
        self.empty_cart_message = (By.XPATH, "//strong[contains(text(),'You have no items in your shopping cart.')]")

        # web elements for adding item to card on the detailed page
        self.item_detailed_page_url = 'https://magento2-demo.magebit.com/beaumont-summit-kit.html'
        self.item_name = (By.XPATH, "//span[@id='product-price-278']")
        self.price_of_item = (By.XPATH, "//span[@id='product-price-278']")
        self.size_l = (By.XPATH, "//div[@aria-label='L']")
        self.color_black = (By.XPATH, "//div[@aria-label='Red']")
        self.quantity = (By.XPATH, "//input[@id='qty']")
        self.detailed_page_add_to_cart_button = (By.XPATH, "//button[@id='product-addtocart-button']")
        self.proceed_to_checkout_button = (By.XPATH, "//button[@id='top-cart-btn-checkout']")
        self.validation_message_about_size = (By.XPATH, "//div[@class='swatch-attribute size']//div[contains(@id,"
                                                        "'super_attribute')]")
        self.validation_message_about_color = (By.XPATH, "//div[@class='swatch-attribute size']//div[contains(@id,"
                                                         "'super_attribute')]")

    def add_item_from_main_page(self):
        self.do_hover(self.element_to_buy)
        self.click(self.size_xs)
        self.click(self.color)
        self.click(self.add_to_cart_button)

    def add_2_items_from_detailed_page(self):
        self.open(self.item_detailed_page_url)
        self.click(self.size_l)
        self.click(self.color_black)
        self.clean_input_field(self.quantity)
        self.input(self.quantity, '2')
        self.click(self.detailed_page_add_to_cart_button)

    def check_added_element_counter(self):
        self.element_is_displayed(self.indication_of_added_item)
        return self.get_text(self.indication_of_added_item)

    def delete_item_from_card(self):
        self.click(self.indication_of_added_item)
        self.click(self.delete_button)
        self.click(self.delete_ok_button)

    def emtpy_cart_message(self):
        return self.get_text(self.empty_cart_message)

    def proceed_to_checkout_from_detail_page(self):
        self.open(self.item_detailed_page_url)
        self.click(self.size_l)
        self.click(self.color_black)
        self.click(self.detailed_page_add_to_cart_button)
        self.click(self.indication_of_added_item)
        self.click(self.proceed_to_checkout_button)
