import pytest
from selenium.common import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    URL = 'https://magento2-demo.magebit.com/'
    restriction_text = (By.XPATH, "//*[contains(text(), 'Estimated waiting time')]")

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def find_element(self, locator):
        try:
            self.wait.until(
                EC.all_of(
                    EC.presence_of_element_located(locator),
                    EC.visibility_of_element_located(locator),
                    EC.element_to_be_clickable(locator)
                )
            )
            return self.driver.find_element(*locator)
        except Exception as e:
            print(f"Element {locator} not found or not clickable. Error: {e}")
            return None

    def find_elements(self, locator):
        try:
            elements = self.wait.until(
                EC.presence_of_all_elements_located(locator)
            )
            return elements
        except Exception as e:
            print(f"Elements {locator} not found. Error: {e}")
            return []

    def do_hover(self, locator):
        ActionChains(self.driver).move_to_element(self.driver.find_element(*locator)).perform()

    def open(self, url):
        self.driver.get(url)
        self.five_minutes_restriction(self.restriction_text)

    def click(self, locator):
        self.find_element(locator).click()

    def click_all_elements(self, locator):
        elements = self.find_elements(locator)
        for element in elements:
            element.click()

    def clean_input_field(self, locator):
        self.find_element(locator).clear()

    def input(self, locator, text):
        self.find_element(locator).send_keys(text)

    def get_text(self, locator):
        text = self.find_element(locator).text
        return text

    def get_attr(self, locator, attribute):
        attr = self.find_element(locator).get_attribute(attribute)
        return attr

    def scroll(self, locator):
        self.driver.execute_script("arguments[0].scrollIntoView(true);", locator)

    # sometimes there is a 5-minute restriction, the test will fail if the restriction is active
    def five_minutes_restriction(self, locator):
        try:
            text = self.driver.find_element(*locator)
            if text.is_displayed():
                pytest.fail(reason="Test failed because of 5 minutes restriction")
        except NoSuchElementException:
            pass

    def get_current_url(self):
        return self.driver.current_url

    def wait_until_element_is_not_displayed(self, locator):
        self.wait.until(EC.invisibility_of_element_located(locator))

    def element_is_displayed(self, locator):
        return self.find_element(locator).is_displayed()

    def refresh_page(self):
        self.driver.refresh()

    def switch_to_frame(self, locator):
        self.driver.switch_to.frame(self.find_element(locator))

    def element_is_selected(self, locator):
        return self.find_element(locator).is_selected()

    def accept_alert(self):
        self.driver.switch_to.alert.accept()
