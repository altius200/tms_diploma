import pytest

from src.Actions.user_creation import User
from src.Pages.CheckoutPage import CheckoutPage
from src.Pages.PurchasePage import PurchasePage
from src.Pages.SearchPage import SearchPage
from src.Pages.Login_Logout_page import LoginPage
from src.Pages.Main_page import MainPage
from src.Pages.Registration_page import RegistrationPage


class BaseTest:
    fake_user: User

    main_page: MainPage
    login_page: LoginPage
    registration_page: RegistrationPage
    search_page: SearchPage
    purchase_page: PurchasePage
    checkout_page: CheckoutPage

    @pytest.fixture(autouse=True)
    def setup(self, request, driver):
        request.cls.driver = driver

        request.cls.main_page = MainPage(driver)
        request.cls.login_page = LoginPage(driver)
        request.cls.registration_page = RegistrationPage(driver)
        request.cls.search_page = SearchPage(driver)
        request.cls.purchase_page = PurchasePage(driver)
        request.cls.checkout_page = CheckoutPage(driver)

